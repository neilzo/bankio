const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const extractLess = new ExtractTextPlugin({
    filename: 'index.css',
    allChunks: true,
});

module.exports = {
    entry: ['./src/js/index.js', './src/less/base.less'],
    output: {
        path: path.join(__dirname,'dist/public'),
        filename: 'bundle.js',
    },
    devServer: {
        historyApiFallback: true,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                use: extractLess.extract({
                    use: [{
                        loader: 'css-loader',
                    }, {
                        loader: 'less-loader',
                    }],
                    fallback: 'style-loader',
                }),
            },
        ],
    },
    plugins: [
        extractLess,
    ],
    resolve: {
        extensions: ['.js', '.jsx'],
    },
};
