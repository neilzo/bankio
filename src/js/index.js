import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Link,
    Route,
} from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';
import { Menu, Container } from 'semantic-ui-react';


import initializeStore from './store/store';
import initialState from './store/initialState';

// Pages
import Home from './pages/Home';
import Transaction from './pages/Transaction';

const store = initializeStore(initialState);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <div>
                <Menu>
                    <Container>
                        <Menu.Item><Link to="/">Balance & History</Link></Menu.Item>
                        <Menu.Item><Link to="/transaction">Make a Transaction</Link></Menu.Item>
                    </Container>
                </Menu>
                <Route exact path="/" component={Home} />
                <Route path="/transaction" component={Transaction} />
            </div>
        </Router>
    </Provider>,
    document.getElementById('root'),
);
