import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import TransactionRow from './TransactionRow';

const Balance = ({ total, transactions }) => (
    <section>
        <h3>Your Balance:</h3>
        <p className="balance">${total.toFixed(2)}</p>
        <h3>Your History:</h3>
        <div className="transaction-wrap">
            {transactions.map((transaction, i) => (
                <TransactionRow transaction={transaction} key={`transaction-${i}`} />
            ))}
        </div>
    </section>
);

Balance.propTypes = {
    total: PropTypes.number,
    transactions: PropTypes.array,
};

Balance.defaultProps = {
    total: 0,
    transactions: [],
};

const mapStateToProps = (state) => {
    return {
        total: state.total,
        transactions: state.transactions,
    };
};

export default connect(mapStateToProps, null)(Balance);
