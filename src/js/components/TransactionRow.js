import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';

const TransactionIcon = ({ transaction }) => {
    return transaction > 0 ? <Icon name="plus" /> : <Icon name="minus" />;
};

TransactionIcon.propTypes = {
    transaction: PropTypes.number,
};

const TransactionRow = ({ transaction }) => (
    <div className="transaction-row">
        <TransactionIcon transaction={transaction} />
        <div className="row-content">
            <p className="row-title">{transaction > 0 ? 'Deposit' : 'Withdrawal'}</p>
            {/* Filler to make this prettier */}
            <p className="text-light">{new Date().toJSON().slice(0, 10)}</p>
        </div>
        <div className="row-content text-right">
            <span className="row-amount">${transaction}</span>
        </div>
    </div>
);

TransactionRow.propTypes = {
    transaction: PropTypes.number,
};

TransactionRow.defaultProps = {
    transaction: 0,
};

export default TransactionRow;
