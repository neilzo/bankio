import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Message, Radio } from 'semantic-ui-react';
import { connect } from 'react-redux';

import * as TransactionsActions from '../actions/transactions';

class DepositForm extends Component {
    static propTypes = {
        createTransaction: PropTypes.func.isRequired,
        total: PropTypes.number,
    };

    static defaultProps = {
        createTransaction() {},
        total: 0,
    };

    static defaultState() {
        return {
            amount: '',
            tType: 'deposit',
            error: false,
            errorMsg: '',
        };
    }

    state = DepositForm.defaultState();

    handleChange = (e, { name, value }) => this.setState({ [name]: value });

    handleSubmit = (e) => {
        e.preventDefault();
        const { createTransaction, total } = this.props;
        const { amount, tType } = this.state;
        const cleanedAmount = Number(amount);
        const isWithdrawal = tType === 'withdraw';

        if (Number.isNaN(cleanedAmount) || parseFloat(cleanedAmount) < 0) {
            this.setState({ error: true, errorMsg: 'Please enter a valid number.' });
            return;
        }

        if (isWithdrawal && cleanedAmount > total) {
            this.setState({ error: true, errorMsg: 'You can\'t withdraw more than you have!' });
            return;
        }

        if (isWithdrawal) {
            // Convert to negative for subtraction by addition in reducer
            createTransaction(cleanedAmount * -1);
        } else {
            createTransaction(cleanedAmount);
        }

        this.setState({ amount: '', error: false, tType: 'deposit', errorMsg: '' });
    };

    handleClear = () => {
        this.setState(DepositForm.defaultState());
    };

    render() {
        const { total } = this.props;
        const { amount, error, tType, errorMsg } = this.state;
        return (
            <Form onSubmit={this.handleSubmit} error={error}>
                <Message
                    error
                    header="Error"
                    content={errorMsg}
                />
                <div>
                    <h3>Balance:</h3>
                    <p className="balance">${total.toFixed(2)}</p>
                </div>
                <div className="form-wrap">
                    <Form.Field>
                        <Form.Input
                            label="Enter Amount"
                            type="text"
                            name="amount"
                            value={amount}
                            onChange={this.handleChange}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Radio
                            name="tType"
                            value="deposit"
                            label="Deposit"
                            checked={tType === 'deposit'}
                            onChange={this.handleChange}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Radio
                            name="tType"
                            value="withdraw"
                            label="Withdraw"
                            checked={tType === 'withdraw'}
                            onChange={this.handleChange}
                        />
                    </Form.Field>
                </div>
                <div className="btn-footer">
                    <Button type="button" secondary onClick={this.handleClear}>Clear</Button>
                    <Button primary type="submit">Submit</Button>
                </div>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        total: state.total,
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        createTransaction(transaction) {
            return dispatch(TransactionsActions.CreateTransaction(transaction));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DepositForm);
