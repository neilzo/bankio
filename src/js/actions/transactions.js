import * as TransactionsConstants from '../constants/transactions';

const SuccessfulCreateTransaction = (transaction) => {
    return {
        type: TransactionsConstants.CREATE_TRANSACTION,
        data: transaction,
    };
};

export const CreateTransaction = (transaction) => {
    return (dispatch) => {
        return dispatch(SuccessfulCreateTransaction(transaction));
    };
};
