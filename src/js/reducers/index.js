import { combineReducers } from 'redux';

import transactions from './transactions';
import total from './total';

export const rootReducer = combineReducers({
    transactions,
    total,
});
