import * as TransactionsActions from '../constants/transactions';

const total = (state = {}, action = {}) => {
    switch (action.type) {
        case TransactionsActions.CREATE_TRANSACTION: {
            const lastTransaction = action.data;
            return state + lastTransaction;
        }
        default:
            return state;
    }
};

export default total;