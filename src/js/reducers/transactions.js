import * as TransactionsActions from '../constants/transactions';

const transactions = (state = {}, action = {}) => {
    switch (action.type) {
        case TransactionsActions.CREATE_TRANSACTION: {
            const clonedState = state.slice();
            clonedState.push(action.data); // +/- dollar amount
            return clonedState;
        }
        default:
            return state;
    }
};

export default transactions;
