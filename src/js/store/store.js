import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from '../reducers';

const finalCreateStore = compose(
    applyMiddleware(thunk),
    typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f,
)(createStore);

const initializeStore = (initialState) => {
    const enhancedInitialState = rootReducer(initialState);

    const store = finalCreateStore(rootReducer, enhancedInitialState);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers/index').rootReducer; // eslint-disable-line global-require
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
};

export default initializeStore;
