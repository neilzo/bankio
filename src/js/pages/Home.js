import React from 'react';
import { Container } from 'semantic-ui-react';
import Balance from '../components/Balance';

const Home = () => (
    <Container>
        <h1>Balance and History</h1>
        <Balance />
    </Container>
);

export default Home;
