import React from 'react';
import { Container } from 'semantic-ui-react';

import TransactionForm from '../components/TransactionForm';

const Transaction = () => (
    <Container>
        <h1>Make a Transaction</h1>
        <TransactionForm />
    </Container>
);

export default Transaction;
