import TransactionsReducer from '../../src/js/reducers/transactions';
import * as TransactionsConstants from '../../src/js/constants/transactions';

it('should return an empty state', () => {
    expect(TransactionsReducer(undefined, {})).toEqual({});
});

it('should push a successful deposit', () => {
    const expectedState = [50];

    expect(TransactionsReducer([], {
        type: TransactionsConstants.CREATE_TRANSACTION,
        data: 50,
    })).toEqual(expectedState);
});

it('should maintain previous state on a transaction', () => {
    const initialState = [10];
    const expectedState = [10, 50];

    expect(TransactionsReducer(initialState, {
        type: TransactionsConstants.CREATE_TRANSACTION,
        data: 50,
    })).toEqual(expectedState);
});
