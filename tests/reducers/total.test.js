import TotalReducer from '../../src/js/reducers/total';
import * as TransactionsConstants from '../../src/js/constants/transactions';

it('should return an empty state', () => {
    expect(TotalReducer(undefined, {})).toEqual({});
});

it('should add to the total on deposit', () => {
    const expectedState = 50;

    expect(TotalReducer(0, {
        type: TransactionsConstants.CREATE_TRANSACTION,
        data: 50,
    })).toEqual(expectedState);
});

it('should subtract from the total on withdrawal', () => {
    const expectedState = 50;

    expect(TotalReducer(100, {
        type: TransactionsConstants.CREATE_TRANSACTION,
        data: -50,
    })).toEqual(expectedState);
});
