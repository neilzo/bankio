module.exports = {
    "env": {
        "browser": true
    },
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "parser": "babel-eslint",
    "rules": {
        "indent": ["error", 4, {
            SwitchCase: 1
        }],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "semi": [
            "error",
            "always"
        ],
        "arrow-body-style": ["off"],
        "no-trailing-spaces": ["error", {
            "skipBlankLines": true
        }],
        "max-len": ["error", 120],
        "no-underscore-dangle": ["error", { allowAfterThis: true, allow: ["_source"]}],
        "no-empty": ["error", { "allowEmptyCatch": true }],
        "no-plusplus": ["error", { "allowForLoopAfterthoughts": true }],
        "no-unused-expressions": ["error", { "allowShortCircuit": true }],
        "import/no-named-as-default": ["off"],
        "import/prefer-default-export": ["off"],
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
        "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
        'jsx-a11y/href-no-hash': ['off'],
        'jsx-a11y/no-static-element-interactions': ['off'],
    },
};
